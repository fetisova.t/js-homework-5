'use strict';
/**
 * Created on 23.06.2019.
 */
//Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
//Экранирование необходимо для того, чтобы интерпретатор не воспринимал символы в тексте как спец символы, например кавычка в строке 'I'm' будет воспринята как конец обозначения строки, сответственно, что делать со второй кавычкой интерпретатору уже не ясно. Для этого используется экранирование.

const firstName =  prompt('Enter your first name:'),
    lastName = prompt('Enter your last name:'),
    birthday = prompt('Enter your date of birth in format dd.mm.yyyy:');

function createNewUser(firstName, lastName, birthday) {
    let newUser = {
        firstName : firstName,
        lastName : lastName,
        birthday : birthday,
        getLogin (){
            return this.firstName[0].toLowerCase()+this.lastName.toLowerCase();
        },
        getAge() {
            let birthDate = this.birthday.split('.');
            birthDate = birthDate[2];
            let currentYear = new Date().getFullYear();
            return currentYear-birthDate;
        },
        getPassword() {
            let birthDate = this.birthday.split('.');
            birthDate = birthDate[2];
            return this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+birthDate;

        }
    };
    return newUser;
}
console.log(createNewUser(firstName, lastName, birthday).getAge());
console.log(createNewUser(firstName, lastName, birthday).getPassword());

